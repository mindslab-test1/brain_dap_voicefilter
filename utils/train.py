import os
import math
import tqdm
import torch
import torch.nn as nn
import itertools
import traceback

from .audio import Audio
from .evaluation import validate
from .utils import get_commit_hash
from model.model import VoiceFilter


def train(args, pt_dir, chkpt_path, trainloader, testloader, writer, logger, hp, hp_str):
    audio = Audio(hp)
    model = VoiceFilter(hp).cuda()
    if hp.train.optimizer == 'adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                     lr=hp.train.adam)
    else:
        raise Exception("%s optimizer not supported" % hp.train.optimizer)

    githash = get_commit_hash()

    init_epoch = -1
    step = 0

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model.load_state_dict(checkpoint['model'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        step = checkpoint['step']
        init_epoch = checkpoint['epoch']
        githash = checkpoint['githash']
        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint. Will use new.")
    else:
        logger.info("Starting new training run")

    torch.backends.cudnn.benchmark = True
    try:
        model.train()
        criterion = nn.MSELoss()
        for epoch in itertools.count(init_epoch+1):
            loader = tqdm.tqdm(trainloader, desc='Train data loader')
            for batch in loader:
                gvlad, target_mag, mixed_mag = batch
                gvlad, target_mag, mixed_mag = \
                    gvlad.cuda(), target_mag.cuda(), mixed_mag.cuda()

                mixed_mag, _ = model.get_mag_phase(mixed_mag)
                target_mag, _ = model.get_mag_phase(target_mag)
                mask = model(mixed_mag, gvlad)
                output = mixed_mag * mask
                loss = criterion(output, target_mag)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                step += 1

                loss = loss.item()
                if loss > 1e8 or math.isnan(loss):
                    logger.error("Loss exploded to %.02f at step %d!" % (loss, step))
                    raise Exception("Loss exploded")

                # write loss to tensorboard
                if step % hp.train.summary_interval == 0:
                    writer.log_training(loss, step)
                    loader.set_description('Loss %.05f at step %d' % (loss, step))

            save_path = os.path.join(pt_dir, '%s_%s_%03d.pt' \
                    % (args.name, githash, epoch))
            torch.save({
                'model': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'step': step,
                'epoch': epoch,
                'hp_str': hp_str,
                'githash': githash,
            }, save_path)
            logger.info("Saved checkpoint to: %s" % save_path)
            validate(audio, model, testloader, writer, step)

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
