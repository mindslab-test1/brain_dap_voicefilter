# adapted from Keith Ito's tacotron implementation
# https://github.com/keithito/tacotron/blob/master/util/audio.py

import torch
import librosa
import numpy as np


class Audio():
    def __init__(self, hp):
        self.hp = hp

    def spec2wav(self, mag, phase):
        # mag: from enhanced output
        # phase: from noisy input.
        phase = np.exp(1j * phase)
        return self.istft(mag, phase)

    def istft(self, mag, phase):
        stft_matrix = mag * phase
        return librosa.istft(stft_matrix,
                             hop_length=self.hp.audio.hop_length,
                             win_length=self.hp.audio.win_length)
