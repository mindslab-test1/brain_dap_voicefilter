import torch
import torch.nn as nn
import torch.nn.functional as F


class AdaptiveInstNorm(nn.Module):
    def __init__(self, hp, channel):
        super(AdaptiveInstNorm, self).__init__()
        self.hp = hp
        self.affine = nn.Linear(hp.model.emb_dim, 2 * channel)

    def forward(self, x, gvlad):
        # x: [B, C, T], vglad: [B, emb_dim]
        ys, yb = torch.chunk(self.affine(gvlad), chunks=2, dim=1)  # [B, C], [B, C]
        norm = F.instance_norm(x) # [B, C, T]
        return ys.unsqueeze(2).expand_as(norm) * norm + yb.unsqueeze(2).expand_as(norm)
        
