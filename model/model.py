import torch
import torch.nn as nn
import torch.nn.functional as F

from .adain import AdaptiveInstNorm


class VoiceFilter(nn.Module):
    def __init__(self, hp):
        super(VoiceFilter, self).__init__()
        self.hp = hp
        assert hp.audio.n_fft // 2 + 1 == hp.audio.num_freq == hp.model.fc2_dim, \
            "stft-related dimension mismatch"
        freq = hp.audio.num_freq

        self.stem_conv = nn.Sequential(
            nn.Conv1d(freq, 512, kernel_size=17, padding=8, dilation=1),
            nn.BatchNorm1d(512), nn.LeakyReLU(),
        )

        self.pre_conv = nn.Sequential(
            nn.Conv1d(512, 512, kernel_size=5, padding=2, dilation=1),
            nn.BatchNorm1d(512), nn.LeakyReLU(),

            nn.Conv1d(512, 512, kernel_size=5, padding=4, dilation=2),
            nn.BatchNorm1d(512), nn.LeakyReLU(),

            nn.Conv1d(512, 512, kernel_size=5, padding=8, dilation=4),
            nn.BatchNorm1d(512), nn.LeakyReLU(),

            nn.Conv1d(512, 512, kernel_size=5, padding=16, dilation=8),
            nn.BatchNorm1d(512), nn.LeakyReLU(),

            nn.Conv1d(512, 512, kernel_size=5, padding=32, dilation=16),
            nn.BatchNorm1d(512), nn.LeakyReLU(),

            nn.Conv1d(512, 512, kernel_size=1),
            nn.BatchNorm1d(512), nn.LeakyReLU(),
        )

        self.conv1 = nn.Conv1d(512, 512, kernel_size=5, padding=2, dilation=1)
        self.adain1 = AdaptiveInstNorm(hp, 512)
        self.conv2 = nn.Conv1d(512, 512, kernel_size=5, padding=4, dilation=2)
        self.adain2 = AdaptiveInstNorm(hp, 512)
        self.conv3 = nn.Conv1d(512, 512, kernel_size=5, padding=8, dilation=4)
        self.adain3 = AdaptiveInstNorm(hp, 512)
        self.conv4 = nn.Conv1d(512, 512, kernel_size=5, padding=16, dilation=8)
        self.adain4 = AdaptiveInstNorm(hp, 512)
        self.conv5 = nn.Conv1d(512, 512, kernel_size=5, padding=32, dilation=16)
        self.adain5 = AdaptiveInstNorm(hp, 512)
        self.conv6 = nn.Conv1d(512, 512, kernel_size=1)
        self.adain6 = AdaptiveInstNorm(hp, 512)

        self.fc = nn.Sequential(
            nn.Linear(512, 512),
            nn.LeakyReLU(),
            nn.Linear(512, 512),
            nn.LeakyReLU(),
            nn.Linear(512, hp.model.fc1_dim),
            nn.LeakyReLU(),
            nn.Linear(hp.model.fc1_dim, hp.model.fc1_dim),
            nn.LeakyReLU(),
            nn.Linear(hp.model.fc1_dim, hp.model.fc2_dim),
        )

        self.window = torch.hann_window(window_length=hp.audio.win_length)

    def get_mag_phase(self, x):
        x = torch.stft(x, n_fft=self.hp.audio.n_fft,
                       hop_length=self.hp.audio.hop_length,
                       win_length=self.hp.audio.win_length,
                       window=self.window) # [B, num_freq, T, 2]
        mag = torch.norm(x, p=2, dim=-1) # [B, num_freq, T]
        mag = self.pre_spec(mag)
        phase = torch.atan2(x[:, :, :, 1], x[:, :, :, 0]) # [B, num_freq, T]
        return mag, phase

    def pre_spec(self, x):
        return self.normalize(self.amp_to_db(x) - self.hp.audio.ref_level_db)

    def post_spec(self, x):
        return self.db_to_amp(self.denormalize(x) + self.hp.audio.ref_level_db)

    def amp_to_db(self, x):
        return 20.0 * torch.log10(torch.max(torch.tensor([1e-5]).cuda(), x))

    def db_to_amp(self, x):
        return torch.pow(torch.tensor([10.0]).cuda(), 0.05*x)

    def normalize(self, x):
        return torch.clamp(x / -self.hp.audio.min_level_db, -1.0, 0.0) + 1.0

    def denormalize(self, x):
        return (torch.clamp(x, 0.0, 1.0) - 1.0) * -self.hp.audio.min_level_db

    def forward(self, x, gvlad):
        x = self.stem_conv(x) # [B, 512, T]
        x = x + self.pre_conv(x) # [B, 512, T]
        identity = x
        x = F.leaky_relu(self.adain1(self.conv1(x), gvlad))
        x = F.leaky_relu(self.adain2(self.conv2(x), gvlad))
        x = F.leaky_relu(self.adain3(self.conv3(x), gvlad))
        x = F.leaky_relu(self.adain4(self.conv4(x), gvlad))
        x = F.leaky_relu(self.adain5(self.conv5(x), gvlad))
        x = F.leaky_relu(self.adain6(self.conv6(x), gvlad))
        x = x + identity

        x = x.transpose(1, 2).contiguous() # [B, T, 512]

        x = self.fc(x) # [B, T, fc2_dim], fc2_dim == num_freq
        x = torch.sigmoid(x)
        x = x.transpose(1, 2) # x: [B, fc2_dim, T]
        return x
