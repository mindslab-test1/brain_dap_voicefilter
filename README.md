# brain_dap_voicefilter
    
brain-dap 내부의 voicefilter(음성분리) 파트 재구성

주의: 학습 코드만 있고, 실행용 grpc packaing 은 되어있지 않음

- `brain_idl` version: 1.0.0
- 관련 컨플루언스 글: https://pms.maum.ai/confluence/x/lnaqAQ

## Author

브레인 박승원 수석
