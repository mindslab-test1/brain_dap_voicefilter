import os
import glob
import torch
import librosa
import argparse
from utils.utils import read_wav_np

from utils.audio import Audio
from utils.hparams import HParam
from model.model import VoiceFilter


def main(args, hp):
    model = VoiceFilter(hp).cuda()
    chkpt_model = torch.load(args.checkpoint_path)['model']
    model.load_state_dict(chkpt_model)
    model.eval()
    audio = Audio(hp)

    gvlad = torch.load(args.reference_file).cuda()
    gvlad = gvlad.unsqueeze(0)
    sr, mixed = read_wav_np(args.mixed_file)
    mixed = torch.from_numpy(mixed).cuda()
    mixed = mixed.unsqueeze(0)
    mixed_mag, mixed_phase = model.get_mag_phase(mixed)

    est_mask = model(mixed_mag, gvlad)
    est_mag = est_mask * mixed_mag
    est_mag = model.post_spec(est_mag)

    est_mag = est_mag[0].cpu().detach().numpy()
    mixed_phase = mixed_phase[0].cpu().detach().numpy()

    est_wav = audio.spec2wav(est_mag, mixed_phase)
    os.makedirs(args.out_dir, exist_ok=True)
    out_path = os.path.join(args.out_dir, 'result.wav')
    librosa.output.write_wav(out_path, est_wav, sr=16000)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for configuration")
    parser.add_argument('-p', '--checkpoint_path', type=str, default=None,
                        help="path of checkpoint pt file")
    parser.add_argument('-m', '--mixed_file', type=str, required=True,
                        help='path of mixed wav file')
    parser.add_argument('-r', '--reference_file', type=str, required=True,
                        help='path of reference gvlad file')
    parser.add_argument('-o', '--out_dir', type=str, required=True,
                        help='directory of output')

    args = parser.parse_args()
    hp = HParam(args.config)

    main(args, hp)
