import os
import glob
import torch
import random
import numpy as np
from utils.utils import read_wav_np
from torch.utils.data import Dataset, DataLoader


def create_dataloader(hp, args, train):
    if train:
        return DataLoader(dataset=VFDataset(hp, args, True),
                          batch_size=hp.train.batch_size,
                          shuffle=True,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=True,
                          )
    else:
        return DataLoader(dataset=VFDataset(hp, args, False),
                          batch_size=1,
                          shuffle=False,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=False,
                          )


class VFDataset(Dataset):
    def __init__(self, hp, args, train):
        self.hp = hp
        self.args = args
        self.train = train
        self.data_dir_list = hp.data.train_dir if train else hp.data.test_dir
        self.spk_dir_list = list()
        for data_dir in self.data_dir_list:
            self.spk_dir_list.extend(glob.glob(os.path.join(data_dir, '*'), recursive=True))
        self.wavpath_list = [glob.glob(os.path.join(spk_dir, '**', '*.wav'), recursive=True)
                            for spk_dir in self.spk_dir_list]
        self.wavpath_list = [x for x in self.wavpath_list if len(x) >= 2]

        self.L = int(self.hp.audio.sr * self.hp.data.audio_len)

    def __len__(self):
        return self.hp.data.steps_per_epoch * self.hp.train.batch_size

    def cut_wav(self, wav):
        samples = len(wav)
        if samples < self.L:
            start = random.randint(0, self.L - samples)
            # if shorter than desired segment length(3.0s), then let's pad with zero
            wav = np.pad(wav, (start, self.L - samples - start),
                    'constant', constant_values=0.0)
            return wav
        else:
            start = random.randint(0, samples - self.L)
            wav = wav[start:start+self.L]
            return wav

    def __getitem__(self, idx):
        spk1, spk2 = random.sample(self.wavpath_list, 2)
        s1_gvlad, s1_target = random.sample(spk1, 2)
        s2_noise = random.choice(spk2)
        s1_gvlad = s1_gvlad.replace('.wav', '_vec.pt')

        sr, w1 = read_wav_np(s1_target)
        sr, w2 = read_wav_np(s2_noise)

        w1 = self.cut_wav(w1)
        w2 = self.cut_wav(w2)
        assert len(w1) == len(w2), "length mismatch even after preprocessing"

        mixed = w1 + w2
        norm = np.max(np.abs(np.concatenate((w1, w2, mixed))))
        w1, w2, mixed = w1/norm, w2/norm, mixed/norm

        w1 = torch.from_numpy(w1)
        w2 = torch.from_numpy(w2)
        mixed = torch.from_numpy(mixed)
        
        gvlad = torch.load(s1_gvlad, map_location='cpu')

        return gvlad, w1, mixed
